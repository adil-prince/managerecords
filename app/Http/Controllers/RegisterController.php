<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index()
    {
        $data['title'] = 'Create Account';
        return view('UserView.Modules.Register', $data);
    }
}
