<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $data['title'] = 'Manage Schedule System | Home';
        return view('UserView.Modules.index', $data);
    }
}
