<header>
    <!-- start navigation -->
    <nav class="navbar navbar-default bootsnav bg-transparent navbar-scroll-top nav-box-width header-light">
        <div class="container-fluid nav-header-container">
            <div class="row">
                <!-- start logo -->
                <div class="col-md-2 col-xs-5">
                    <a href="index.html" title="Pofo" class="logo"><img src="{{asset('public/assets/images/logo.png')}}" data-rjs="{{asset('public/assets/images/logo@2x.png')}}" class="logo-dark default" alt="Pofo"><img src="{{asset('public/assets/images/logo-white.png')}}" data-rjs="{{asset('public/assets/images/logo-white@2x.png')}}" alt="Pofo" class="logo-light"></a>
                </div>
                <!-- end logo -->
                <div class="col-md-7 col-xs-2 width-auto pull-right accordion-menu xs-no-padding-right">
                    <button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#navbar-collapse-toggle-1">
                        <span class="sr-only">toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-collapse collapse pull-right" id="navbar-collapse-toggle-1">
                        <ul id="accordion" class="nav navbar-nav navbar-left no-margin alt-font text-normal" data-in="fadeIn" data-out="fadeOut">
                            <li class="dropdown megamenu-fw">
                                <a href="{{URL::to('')}}">Home</a>
                            </li>
                            <li class="dropdown simple-dropdown">
                                <a href="{{URL::to('Schedules')}}">Schedules</a>
                            </li>
                            <li class="dropdown megamenu-fw">
                                <a href="{{URL::to('Accounts')}}">Sale & Purchase</a>
                            </li>
                            <li class="dropdown megamenu-fw">
                                <a href="{{URL::to('Create-Account')}}">Create Account</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- end navigation -->
</header>