<!-- start header -->
<header>
    <!-- start navigation -->
    <nav class="navbar navbar-default bootsnav navbar-top header-dark bg-transparent">
        <div class="container nav-header-container">
            <div class="row">
                <!-- start logo -->
                <div class="col-md-2 col-xs-5">
                    <a href="index.html" title="Pofo" class="logo"><img src="{{asset('public/assets/images/logo.png')}}" data-rjs="{{asset('public/assets/images/logo@2x.png')}}" class="logo-dark default" alt="Pofo"><img src="{{asset('public/assets/images/logo-white.png')}}" data-rjs="{{asset('public/assets/images/logo-white@2x.png')}}" alt="Pofo" class="logo-light"></a>
                </div>
                <!-- end logo -->
                <div class="col-md-7 col-xs-2 width-auto pull-right accordion-menu xs-no-padding-right">
                    <button type="button" class="navbar-toggle collapsed pull-right" data-toggle="collapse" data-target="#navbar-collapse-toggle-1">
                        <span class="sr-only">toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="navbar-collapse collapse pull-right" id="navbar-collapse-toggle-1">
                        <ul id="accordion" class="nav navbar-nav navbar-left no-margin alt-font text-normal" data-in="fadeIn" data-out="fadeOut">
                            <li class="dropdown megamenu-fw">
                                <a href="{{URL::to('')}}">Home</a>
                            </li>
                            <li class="dropdown simple-dropdown">
                                <a href="{{URL::to('Schedules')}}">Schedules</a>
                            </li>
                            <li class="dropdown megamenu-fw">
                                <a href="{{URL::to('Accounts')}}">Sale & Purchase</a>
                            </li>
                            <li class="dropdown megamenu-fw">
                                <a href="{{URL::to('Create-Account')}}">Create Account</a>
                            </li>
                            <!-- <li class="dropdown simple-dropdown"><a href="javascript:void(0);" title="Blog">Blog</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>

                                <ul class="dropdown-menu" role="menu">
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Blog Standard <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-standard-full-width.html">Blog standard – full width</a></li>
                                            <li><a href="blog-standard-with-left-sidebar.html">Blog standard with left sidebar</a></li>
                                            <li><a href="blog-standard-with-right-sidebar.html">Blog standard with right sidebar</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Blog Classic <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-classic.html">Blog classic</a></li>
                                            <li><a href="blog-classic-full-width.html">Blog classic – full width</a></li>

                                        </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Blog List <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-list.html">Blog list</a></li>
                                            <li><a href="blog-list-full-width.html">Blog list – full width</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Blog Grid <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-grid.html">Blog grid</a></li>
                                            <li><a href="blog-grid-full-width.html">Blog grid – full width</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Blog Masonry <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-masonry.html">Blog masonry</a></li>
                                            <li><a href="blog-masonry-full-width.html">Blog masonry – full width</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Blog Simple <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-simple.html">Blog simple</a></li>
                                            <li><a href="blog-simple-full-width.html">Blog simple – full width</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Blog Clean <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-clean.html">Blog clean</a></li>
                                            <li><a href="blog-clean-full-width.html">Blog clean – full width</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Blog Images <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-images.html">Blog images</a></li>
                                            <li><a href="blog-images-full-width.html">Blog images – full width</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Blog Only Text <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-only-text.html">Blog only text</a></li>
                                            <li><a href="blog-only-text-full-width.html">Blog only text – full width</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Post Layout <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-post-layout-01.html">Blog post layout 01</a></li>
                                            <li><a href="blog-post-layout-02.html">Blog post layout 02</a></li>
                                            <li><a href="blog-post-layout-03.html">Blog post layout 03</a></li>
                                            <li><a href="blog-post-layout-04.html">Blog post layout 04</a></li>
                                            <li><a href="blog-post-layout-05.html">Blog post layout 05</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">Post Types <i class="fas fa-angle-right"></i></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="blog-standard-post.html">Standard post</a></li>
                                            <li><a href="blog-gallery-post.html">Gallery post</a></li>
                                            <li><a href="blog-slider-post.html">Slider post</a></li>
                                            <li><a href="blog-html5-video-post.html">HTML5 video post</a></li>
                                            <li><a href="blog-youtube-video-post.html">Youtube video post</a></li>
                                            <li><a href="blog-vimeo-video-post.html">Vimeo video post</a></li>
                                            <li><a href="blog-audio-post.html">Audio post</a></li>
                                            <li><a href="blog-blockquote-post.html">Blockquote post</a></li>
                                            <li><a href="blog-full-width-post.html">Full width post</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> -->
                            <!-- <li class="dropdown megamenu-fw">
                                <a href="javascript:void(0);">Elements</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                                <div class="menu-back-div dropdown-menu megamenu-content mega-menu collapse mega-menu-full icon-list-menu">
                                    <ul class="equalize sm-equalize-auto">
                                        <li class="mega-menu-column col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <ul>
                                                <li class="dropdown-header">General elements</li>
                                                <li><a href="accordions.html"><i class="ti-layout-accordion-separated"></i> Accordions</a></li>
                                                <li><a href="buttons.html"><i class="ti-mouse"></i> Buttons</a></li>
                                                <li><a href="team.html"><i class="ti-user"></i> Team</a></li>
                                                <li><a href="team-carousel.html"><i class="ti-layout-slider-alt"></i> Team carousel</a></li>
                                                <li><a href="clients.html"><i class="ti-id-badge"></i> Clients</a></li>
                                                <li><a href="client-carousel.html"><i class="ti-layout-slider"></i> Client carousel</a></li>
                                                <li><a href="subscribe.html"><i class="ti-bookmark"></i> Subscribe</a></li>
                                                <li><a href="call-to-action.html"><i class="ti-headphone-alt"></i> Call to action</a></li>
                                                <li><a href="tab.html"><i class="ti-layout-tab"></i> Tab</a></li>
                                                <li><a href="google-map.html"><i class="ti-location-pin"></i> Google map</a></li>
                                                <li><a href="text-slider.html"><i class="ti-layout-media-overlay"></i> Text slider</a></li>
                                                <li><a href="contact-form.html"><i class="ti-clipboard"></i> Contact form</a></li>
                                                <li><a href="image-gallery.html"><i class="ti-gallery"></i> Image gallery</a></li>
                                            </ul>
                                        </li>

                                        <li class="mega-menu-column col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <ul>
                                                <li class="dropdown-header">Infographics / icons</li>
                                                <li><a href="process-bar.html"><i class="icon-hourglass"></i> Process bar</a></li>
                                                <li><a href="icon-with-text.html"><i class="ti-layout-media-left"></i> Icon with text</a></li>
                                                <li><a href="overline-icon-box.html"><i class="ti-layout-placeholder"></i> Overline icon box</a></li>
                                                <li><a href="custom-icon-with-text.html"><i class="ti-layout-cta-btn-left"></i> Custom icon with text</a></li>
                                                <li><a href="counters.html"><i class="ti-timer"></i> Counters</a></li>
                                                <li><a href="countdown.html"><i class="ti-alarm-clock"></i> Countdown</a></li>
                                                <li><a href="pie-charts.html"><i class="ti-pie-chart"></i> Pie charts</a></li>
                                                <li><a href="text-box.html"><i class="ti-layout-cta-left"></i> Text box</a></li>
                                                <li><a href="fancy-text-box.html"><i class="ti-layout-cta-center"></i> Fancy text box</a></li>
                                            </ul>
                                        </li>

                                        <li class="mega-menu-column col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <ul>
                                                <li class="dropdown-header">Interactive Elements</li>
                                                <li><a href="testimonials.html"><i class="ti-thought"></i> Testimonials</a></li>
                                                <li><a href="testimonials-carousel.html"><i class="ti-comments"></i> Testimonials carousel</a></li>
                                                <li><a href="video.html"><i class="ti-video-camera"></i> Video</a></li>
                                                <li><a href="interactive-banners.html"><i class="ti-image"></i> Interactive banners</a></li>
                                                <li><a href="services.html"><i class="ti-headphone-alt"></i> Services</a></li>
                                                <li><a href="portfolio-slider.html"><i class="ti-layout-slider-alt"></i> Portfolio slider</a></li>
                                                <li><a href="info-banner.html"><i class="ti-layout-slider"></i> Info banner</a></li>
                                                <li><a href="rotate-box.html"><i class="ti-layout-width-full"></i> Rotate box</a></li>
                                                <li><a href="process-step.html"><i class="ti-stats-up"></i> Process step</a></li>
                                                <li><a href="blog-posts.html"><i class="ti-comment-alt"></i> Blog posts</a></li>
                                                <li><a href="instagram.html"><i class="ti-instagram"></i> Instagram</a></li>
                                                <li><a href="parallax-scrolling.html"><i class="ti-exchange-vertical"></i> Parallax scrolling</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-column col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <ul>
                                                <li class="dropdown-header">Text & containers</li>
                                                <li><a href="heading.html"><i class="ti-text"></i> Heading</a></li>
                                                <li><a href="dropcaps.html"><i class="ti-layout-accordion-list"></i> Dropcaps</a></li>
                                                <li><a href="columns.html"><i class="ti-layout-column3-alt"></i> Columns</a></li>
                                                <li><a href="blockquote.html"><i class="ti-quote-left"></i> Blockquote</a></li>
                                                <li><a href="highlights.html"><i class="ti-underline"></i> Highlights</a></li>
                                                <li><a href="message-box.html"><i class="ti-layout-media-right-alt"></i> Message box</a></li>
                                                <li><a href="social-icons.html"><i class="ti-signal"></i> Social icons</a></li>
                                                <li><a href="lists.html"><i class="ti-list"></i> Lists</a></li>
                                                <li><a href="seperators.html"><i class="ti-layout-line-solid"></i> Separators</a></li>
                                                <li><a href="pricing-table.html"><i class="ti-layout-grid2-thumb"></i> Pricing table</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->
                            <!-- <li class="dropdown megamenu-fw">
                                <a href="javascript:void(0);">Features</a><i class="fas fa-angle-down dropdown-toggle" data-toggle="dropdown" aria-hidden="true"></i>
                                <div class="menu-back-div dropdown-menu megamenu-content mega-menu collapse mega-menu-full">
                                    <ul class="equalize sm-equalize-auto">
                                        <li class="mega-menu-column col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <ul>
                                                <li class="dropdown-header">Header Styles</li>
                                                <li><a href="transparent-header.html">Transparent header</a></li>
                                                <li><a href="white-header.html">White header</a></li>
                                                <li><a href="dark-header.html">Dark header</a></li>
                                                <li><a href="header-with-top-bar.html">Header with top bar</a></li>
                                                <li><a href="header-with-sticky-top-bar.html">Header with sticky top bar</a></li>
                                                <li><a href="header-with-push.html">Header with push</a></li>
                                                <li><a href="center-navigation.html">Center navigation</a></li>
                                                <li><a href="center-logo.html">Center logo</a></li>
                                                <li><a href="top-logo.html">Top logo</a></li>
                                                <li><a href="one-page-navigation.html">One page navigation</a></li>
                                                <li><a href="hamburger-menu.html">Hamburger menu</a></li>
                                                <li><a href="hamburger-menu-modern.html">Hamburger menu modern</a></li>
                                                <li><a href="hamburger-menu-half.html">Hamburger menu half</a></li>
                                                <li><a href="left-menu-classic.html">Left menu classic</a></li>
                                                <li><a href="left-menu-modern.html">Left menu modern</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-column col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <ul>
                                                <li class="dropdown-header">Footer</li>
                                                <li><a href="footer-standard.html">Footer standard</a></li>
                                                <li><a href="footer-standard-dark.html">Footer standard dark</a></li>
                                                <li><a href="footer-classic.html">Footer classic</a></li>
                                                <li><a href="footer-classic-dark.html">Footer classic dark</a></li>
                                                <li><a href="footer-clean.html">Footer clean</a></li>
                                                <li><a href="footer-clean-dark.html">Footer clean dark</a></li>
                                                <li><a href="footer-modern.html">Footer modern</a></li>
                                                <li><a href="footer-modern-dark.html">Footer modern dark</a></li>
                                                <li><a href="footer-center-logo.html">Footer center logo </a></li>
                                                <li><a href="footer-center-logo-dark.html">Footer center logo dark</a></li>
                                                <li><a href="footer-strip.html">Footer strip</a></li>
                                                <li><a href="footer-strip-dark.html">Footer strip dark</a></li>
                                                <li><a href="footer-center-logo-02.html">Footer center logo 02</a></li>
                                                <li><a href="footer-center-logo-02-dark.html">Footer center logo 02 dark</a></li>
                                            </ul>
                                        </li>

                                        <li class="mega-menu-column col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <ul>
                                                <li class="dropdown-header">Page Title</li>
                                                <li><a href="page-title-left-alignment.html">Left alignment</a></li>
                                                <li><a href="page-title-right-alignment.html">Right alignment</a></li>
                                                <li><a href="page-title-center-alignment.html">Center alignment</a></li>
                                                <li><a href="page-title-dark-style.html">Dark style</a></li>
                                                <li><a href="page-title-big-typography.html">Big typography</a></li>
                                                <li><a href="page-title-parallax-image-background.html">Parallax image background</a></li>
                                                <li><a href="page-title-background-breadcrumbs.html">Image after breadcrumbs</a></li>
                                                <li><a href="page-title-gallery-background.html">Gallery background</a></li>
                                                <li><a href="page-title-background-video.html">Background video</a></li>
                                                <li><a href="page-title-mini-version.html">Mini version</a></li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu-column col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <ul>
                                                <li class="dropdown-header">Gallery</li>
                                                <li><a href="single-image-lightbox.html">Single image lightbox</a></li>
                                                <li><a href="lightbox-gallery.html">Lightbox gallery</a></li>
                                                <li><a href="zoom-gallery.html">Zoom gallery</a></li>
                                                <li><a href="popup-with-form.html">Popup with form</a></li>
                                                <li><a href="modal-popup.html">Modal popup</a></li>
                                                <li><a href="open-youtube-video.html">Open youtube video</a></li>
                                                <li><a href="open-vimeo-video.html">Open vimeo video</a></li>
                                                <li><a href="open-google-map.html">Open google map</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </li> -->
                        </ul>
                    </div>
                </div>
                <!-- <div class="col-md-2 col-xs-5 width-auto">
                    <div class="header-searchbar">
                        <a href="#search-header" class="header-search-form"><i class="fas fa-search search-button"></i></a>

                        <form id="search-header" method="post" action="http://www.themezaa.com/html/pofo/search-result.html" name="search-header" class="mfp-hide search-form-result">
                            <div class="search-form position-relative">
                                <button type="submit" class="fas fa-search close-search search-button"></button>
                                <input type="text" name="search" class="search-input" placeholder="Enter your keywords..." autocomplete="off">
                            </div>
                        </form>
                    </div>
                    <div class="heder-menu-button sm-display-none">
                        <button class="navbar-toggle mobile-toggle right-menu-button" type="button" id="showRightPush">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div> -->
            </div>
        </div>
    </nav>
    <!-- end navigation -->
    <!-- start push menu -->
    <div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
        <button class="close-button-menu side-menu-close" id="close-pushmenu"></button>
        <div class="display-table padding-twelve-all height-100 width-100 text-center">
            <div class="display-table-cell vertical-align-top padding-70px-top position-relative">
                <div class="row">
                    <!-- start push menu logo -->
                    <div class="col-lg-12 margin-70px-bottom">
                        <img src="images/logo-black-big.png" alt="" />
                    </div>
                    <!-- end push menu logo -->
                    <div class="col-lg-12 margin-70px-bottom">
                        <img src="images/sidebar-image1.png" alt="" />
                    </div>
                    <div class="col-lg-12">
                        <h5 class="alt-font text-extra-dark-gray"><span class="display-block font-weight-300 text-dark-gray">The world's most</span><strong>powerful website builder.</strong></h5>
                    </div>
                    <div class="col-lg-12">
                        <a href="https://themeforest.net/item/pofo-creative-agency-corporate-and-portfolio-multipurpose-template/20645944?ref=themezaa" target="_blank" class="btn btn-deep-pink btn-small text-extra-small border-radius-4"><i class="fas fa-play-circle icon-very-small margin-5px-right no-margin-left" aria-hidden="true"></i>Purchase Now</a>
                    </div>
                    <!-- start social links -->
                    <div class="col-md-12 margin-100px-top text-center">
                        <div class="icon-social-medium margin-three-bottom">
                            <a href="https://www.facebook.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fab fa-facebook-f" aria-hidden="true"></i></a>
                            <a href="https://twitter.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                            <a href="https://dribbble.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fab fa-dribbble" aria-hidden="true"></i></a>
                            <a href="https://plus.google.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fab fa-google-plus-g" aria-hidden="true"></i></a>
                            <a href="https://www.tumblr.com/" target="_blank" class="text-extra-dark-gray text-deep-pink-hover margin-one-lr"><i class="fab fa-tumblr" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <!-- end social links -->
                </div>
            </div>
        </div>

    </div>
    <!-- end push menu -->

</header>
<!-- end header -->