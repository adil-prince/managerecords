<!-- favicon -->
<link rel="shortcut icon" href="{{asset('public/assets/images/favicon.png')}}">
<link rel="apple-touch-icon" href="{{asset('public/assets/images/apple-touch-icon-57x57.png')}}">
<link rel="apple-touch-icon" sizes="72x72" href="{{asset('public/assets/images/apple-touch-icon-72x72.png')}}">
<link rel="apple-touch-icon" sizes="114x114" href="{{asset('public/assets/images/apple-touch-icon-114x114.png')}}">
<!-- animation -->
<link rel="stylesheet" href="{{asset('public/assets/css/animate.css')}}" />
<!-- bootstrap -->
<link rel="stylesheet" href="{{asset('public/assets/css/bootstrap.min.css')}}" />
<!-- et line icon -->
<link rel="stylesheet" href="{{asset('public/assets/css/et-line-icons.css')}}" />
<!-- font-awesome icon -->
<link rel="stylesheet" href="{{asset('public/assets/css/font-awesome.min.css')}}" />
<!-- themify icon -->
<link rel="stylesheet" href="{{asset('public/assets/css/themify-icons.css')}}">
<!-- swiper carousel -->
<link rel="stylesheet" href="{{asset('public/assets/css/swiper.min.css')}}">
<!-- justified gallery -->
<link rel="stylesheet" href="{{asset('public/assets/css/justified-gallery.min.css')}}">
<!-- magnific popup -->
<link rel="stylesheet" href="{{asset('public/assets/css/magnific-popup.css')}}" />
<!-- revolution slider -->
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/revolution/css/settings.css')}}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/revolution/css/layers.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('public/assets/revolution/css/navigation.css')}}">
<!-- bootsnav -->
<link rel="stylesheet" href="{{asset('public/assets/css/bootsnav.css')}}">
<!-- style -->
<link rel="stylesheet" href="{{asset('public/assets/css/style.css')}}" />
<!-- responsive css -->
<link rel="stylesheet" href="{{asset('public/assets/css/responsive.css')}}" />
<link rel="stylesheet" href="{{asset('public/assets/style.css')}}" />