<footer class="footer-standard-dark bg-extra-green-blue">
    <div class="footer-widget-area padding-five-tb xs-padding-30px-tb">
        <div class="container">
            <div class="row equalize xs-equalize-auto">
                <div class="col-md-3 col-sm-6 col-xs-12 widget text-green-blue border-right border-color-medium-dark-gray sm-no-border-right sm-margin-30px-bottom xs-text-center">
                    <!-- start logo -->
                    <a href="index.html" class="margin-20px-bottom display-inline-block"><img class="footer-logo" src="{{asset('public/assets/images/logo-white.png')}}" data-rjs="{{asset('public/assets/images/logo-white@2x.png')}}" alt="Pofo"></a>
                    <!-- end logo -->
                    <p class="text-small width-95 xs-width-100">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <!-- start social media -->
                    <div class="social-icon-style-8 display-inline-block vertical-align-middle">
                        <ul class="small-icon no-margin-bottom">
                            <li><a class="facebook text-white" href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
                            <li><a class="twitter text-white" href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li><a class="google text-white" href="https://plus.google.com/" target="_blank"><i class="fab fa-google-plus-g"></i></a></li>
                            <li><a class="instagram text-white" href="https://instagram.com/" target="_blank"><i class="fab fa-instagram no-margin-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <!-- end social media -->
                </div>
                <!-- start additional links -->
                <div class="col-md-3 col-sm-6 col-xs-12 widget border-right border-color-medium-dark-gray padding-45px-left sm-padding-15px-left sm-no-border-right sm-margin-30px-bottom xs-text-center">
                    <div class="widget-title alt-font text-small text-green-blue text-uppercase margin-10px-bottom font-weight-600">Additional Links</div>
                    <ul class="list-unstyled">
                        <li><a class="text-small text-green-blue" href="index.html">Home Classic Corporate</a></li>
                        <li><a class="text-small text-green-blue" href="home-creative-business.html">Home Creative Business</a></li>
                        <li><a class="text-small text-green-blue" href="home-creative-designer.html">Home Creative Designer</a></li>
                        <li><a class="text-small text-green-blue" href="home-portfolio-minimal.html">Home Portfolio Minimal</a></li>
                        <li><a class="text-small text-green-blue" href="home-portfolio-parallax.html">Home Portfolio parallax</a></li>
                        <li><a class="text-small text-green-blue" href="home-portfolio-personal.html">Home Portfolio Personal</a></li>
                    </ul>
                </div>
                <!-- end additional links -->
                <!-- start contact information -->
                <div class="col-md-3 col-sm-6 col-xs-12 widget border-right border-color-medium-dark-gray padding-45px-left sm-padding-15px-left sm-clear-both sm-no-border-right  xs-margin-30px-bottom xs-text-center text-green-blue">
                    <div class="widget-title alt-font text-small text-uppercase margin-10px-bottom font-weight-600">Contact Info</div>
                    <p class="text-small display-block margin-15px-bottom width-80 xs-center-col">POFO Design Agency<br> 301 The Greenhouse, Custard Factory, London, E2 8DY.</p>
                    <div class="text-small">Email: <a href="mailto:sales@domain.com" class="text-green-blue">sales@domain.com</a></div>
                    <div class="text-small">Phone: +44 (0) 123 456 7890</div>
                    <a href="contact-us-modern.html" class="text-small text-green-blue text-uppercase text-decoration-underline">View Direction</a>
                </div>
                <!-- end contact information -->
                <!-- start instagram -->
                <div class="col-md-3 col-sm-6 col-xs-12 widget padding-45px-left sm-padding-15px-left xs-text-center">
                    <div class="widget-title alt-font text-small text-green-blue text-uppercase margin-20px-bottom font-weight-600">Instagram portfolio</div>
                    <div class="instagram-follow-api">
                        <ul id="instaFeed-footer"></ul>
                    </div>
                </div>
                <!-- end instagram -->
            </div>
        </div>
    </div>
    <div class="bg-dark-footer padding-50px-tb text-center xs-padding-30px-tb">
        <div class="container">
            <div class="row">
                <!-- start copyright -->
                <div class="col-md-6 col-sm-6 col-xs-12 text-left text-small xs-text-center">&copy; 2017 POFO is Proudly Powered by <a href="http://www.themezaa.com/" target="_blank" class="text-dark-gray">ThemeZaa</a></div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-right text-small xs-text-center">
                    <a href="javascript:void(0);" class="text-dark-gray">Term and Condition</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:void(0);" class="text-dark-gray">Privacy Policy</a>
                </div>
                <!-- end copyright -->
            </div>
        </div>
    </div>
</footer>