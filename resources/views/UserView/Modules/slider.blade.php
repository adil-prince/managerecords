<!-- start slider section -->
<section class="no-padding main-slider height-100 mobile-height wow fadeIn ">
    <div class="swiper-full-screen swiper-container height-100 width-100 black-move">
        <div class="swiper-wrapper">
            <!-- start slider item -->
            <div class="swiper-slide cover-background" style="background-image:url({{asset('public/assets/images/homepage-8-slider-img-3.jpg')}});">
                <div class="container position-relative full-screen">
                    <div class="col-md-12 slider-typography text-left xs-text-center">
                        <div class="slider-text-middle-main">
                            <div class="slider-text-middle">
                                <h1 class="alt-font text-extra-dark-gray font-weight-700 letter-spacing-minus-1 line-height-80 width-55 margin-35px-bottom md-width-60 sm-width-70 md-line-height-auto xs-width-100 xs-margin-15px-bottom">We combine design, thinking and technical</h1>
                                <p class="text-extra-dark-gray text-large margin-four-bottom width-40 md-width-50 sm-width-60 xs-width-100 xs-margin-15px-bottom">Everything you could possibly want it to do and not only that, beautifully carefully designed.</p>
                                <div class="btn-dual"><a href="https://themeforest.net/item/pofo-creative-agency-corporate-and-portfolio-multipurpose-template/20645944?ref=themezaa" target="_blank" class="btn btn-dark-gray btn-rounded btn-small no-margin-lr">Purchase Pofo</a><a href="https://themeforest.net/item/pofo-creative-agency-corporate-and-portfolio-multipurpose-template/20645944?ref=themezaa" target="_blank" class="btn btn-transparent-dark-gray btn-rounded btn-small margin-20px-lr xs-margin-5px-top">Download now</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end slider item -->
            <!-- start slider item -->
            <div class="swiper-slide cover-background" style="background-image:url({{asset('public/assets/images/homepage-8-slider-img-2.jpg')}});">
                <div class="container position-relative full-screen">
                    <div class="col-md-12 slider-typography text-left xs-text-center">
                        <div class="slider-text-middle-main">
                            <div class="slider-text-middle">
                                <h1 class="alt-font text-extra-dark-gray font-weight-700 letter-spacing-minus-1 line-height-80 width-55 margin-35px-bottom md-width-60 sm-width-70 md-line-height-auto xs-width-100 xs-margin-15px-bottom">We always stay on the cutting edge of digital</h1>
                                <p class="text-extra-dark-gray text-large margin-four-bottom width-40 md-width-50 sm-width-60 xs-width-100 xs-margin-15px-bottom">Everything you could possibly want it to do and not only that, beautifully carefully designed.</p>
                                <div class="btn-dual"><a href="https://themeforest.net/item/pofo-creative-agency-corporate-and-portfolio-multipurpose-template/20645944?ref=themezaa" target="_blank" class="btn btn-dark-gray btn-rounded btn-small no-margin-lr">Purchase Pofo</a><a href="https://themeforest.net/item/pofo-creative-agency-corporate-and-portfolio-multipurpose-template/20645944?ref=themezaa" target="_blank" class="btn btn-transparent-dark-gray btn-rounded btn-small margin-20px-lr xs-margin-5px-tb">Download now</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end slider item -->
            <!-- start slider item -->
            <div class="swiper-slide cover-background" style="background-image:url({{asset('public/assets/images/homepage-8-slider-img-1.jpg')}});">
                <div class="container position-relative full-screen">
                    <div class="col-md-12 slider-typography text-left xs-text-center">
                        <div class="slider-text-middle-main">
                            <div class="slider-text-middle">
                                <h1 class="alt-font text-extra-dark-gray font-weight-700 letter-spacing-minus-1 line-height-80 width-55 margin-35px-bottom md-width-60 sm-width-70 md-line-height-auto xs-width-100 xs-margin-15px-bottom">We crafts stunning and intuitive apps</h1>
                                <p class="text-extra-dark-gray text-large margin-four-bottom width-40 md-width-50 sm-width-60 xs-width-100 xs-margin-15px-bottom">Everything you could possibly want it to do and not only that, beautifully carefully designed.</p>
                                <div class="btn-dual"><a href="https://themeforest.net/item/pofo-creative-agency-corporate-and-portfolio-multipurpose-template/20645944?ref=themezaa" target="_blank" class="btn btn-dark-gray btn-rounded btn-small no-margin-lr">Purchase Pofo</a><a href="https://themeforest.net/item/pofo-creative-agency-corporate-and-portfolio-multipurpose-template/20645944?ref=themezaa" target="_blank" class="btn btn-transparent-dark-gray btn-rounded btn-small margin-20px-lr xs-margin-5px-tb">Download now</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end slider item -->
        </div>
        <!-- start slider pagination -->
        <div class="swiper-pagination swiper-full-screen-pagination"></div>
        <div class="swiper-button-next swiper-button-black-highlight display-none"></div>
        <div class="swiper-button-prev swiper-button-black-highlight display-none"></div>
        <!-- end slider pagination -->
    </div>
</section>
<!-- end slider section -->