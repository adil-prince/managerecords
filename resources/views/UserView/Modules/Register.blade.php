@extends('Layouts.BreadCrumLayout')

@section('MainSection')

<section class="wow fadeIn bg-extra-green-blue padding-35px-tb page-title-small top-space">
    <div class="container">
        <div class="row equalize">
            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12 display-table">
                <div class="display-table-cell vertical-align-middle text-left xs-text-center">
                    <!-- start page title -->
                    <h1 class="alt-font text-white font-weight-600 no-margin-bottom text-uppercase">Create Account</h1>
                    <!-- end page title -->
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 display-table text-right xs-text-left xs-margin-10px-top">
                <div class="display-table-cell vertical-align-middle breadcrumb text-small alt-font">
                    <!-- start breadcrumb -->
                    <ul class="xs-text-center">
                        <li><a href="{{URL::to('')}}" class="text-green-blue">Home</a></li>
                        <!-- <li><a href="#" class="text-dark-gray">General elements</a></li> -->
                        <li class="text-green-blue">Create Account</li>
                    </ul>
                    <!-- end breadcrumb -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="wow fadeIn bg-light-gray">
    <div class="container">
        <!-- <div class="row">
            <div class="col-md-7 col-sm-12 col-xs-12 center-col text-center margin-100px-bottom xs-margin-40px-bottom">
                <div class="position-relative overflow-hidden width-100">
                    <span class="text-small text-outside-line-full alt-font font-weight-600 text-uppercase">Contact Form Style 03</span>
                </div>
            </div>
        </div> -->
        <form id="contact-form-3" action="javascript:void(0)" method="post">
            <div class="row">
                <div class="col-md-12 wow fadeIn center-col">
                    <div class="padding-four-all bg-white border-radius-6 md-padding-seven-all">
                        <div class="text-extra-dark-gray alt-font text-large font-weight-600 margin-30px-bottom">Looking for a excellent business idea?</div>
                        <div id="success-contact-form-3" class="no-margin-lr"></div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="name" id="name" placeholder="First Name *" autocomplete="new-text" class="input-bg">
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="name" id="name" placeholder="Last Name *" autocomplete="new-text" class="input-bg">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="email" name="email" id="email" placeholder="E-mail*" autocomplete="new-email" class="input-bg">
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <select name="name" id="name" class="input-bg">
                                    <option value="0">Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="name" id="name" placeholder="Address line 1 *" autocomplete="new-text" class="input-bg">
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="name" id="name" placeholder="Address line 2 *" autocomplete="new-text" class="input-bg">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <select name="country" id="country" class="input-bg">
                                    <option value="0">Select Country</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="City" id="City" placeholder="City" autocomplete="new-text" class="input-bg">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="State" id="State" placeholder="State" autocomplete="new-text" class="input-bg">
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="text" name="Zip" id="Zip" placeholder="Zip Code" autocomplete="new-text" class="input-bg">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="password" name="password" id="email" placeholder="Password*" class="input-bg">
                            </div>
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <input type="password" name="password" id="email" placeholder="Confirn Password*" class="input-bg">
                            </div>
                        </div>
                            <!-- <textarea name="comment" id="comment" placeholder="Your Message" class="input-bg"></textarea> -->
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <button id="contact-us-button-3" type="submit" class="btn btn-small btn-greenblue">Create Account</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>

@endsection