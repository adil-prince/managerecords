<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Schedule | {{$title}}</title>
    @include('UserView.Includes.HeadFiles')
</head>
<body>
    @include('UserView.Includes.Header2')
    @yield('MainSection')
    @include('UserView.Includes.Footer')
    @include('UserView.Includes.FooterFiles')
</body>
</html>